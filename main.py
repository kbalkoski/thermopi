# from core.services.test_relay import test_relay
from core.services.sensor import read_temp, TempSensor, read_temps
from Adafruit_IO import *
import time

READ_DELAY = 5

aio = Client('105b95eb2d3143aebe344b1812a31367')

T1 = TempSensor(name="T1", offset=1)

# test_relay()
print(T1.get_temp())

while True:
    time.sleep(READ_DELAY)
    temperature = T1.get_temp()
    temperature2 = read_temps([T1], ['T1'])
    # data = Data(value=temperature)
    # aio.send('Bedroom', temperature)
    #aio.create_data('Bedroom', data)
    print(temperature)
    print(temperature2)
