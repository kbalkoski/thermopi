# import RPi.GPIO as GPIO
import time
import json
from pprint import pprint
from sensor import TempSensor, read_temps

try:
    import Adafruit_DHT

    test_mode = False
except:
    test_mode = True

DELAY_TIME = 2

# GPIO.setmode(GPIO.BCM)

# Set temperature sensors
T = [TempSensor(name="T0", offset=1)]

# Read JSON
with open('params.json') as params_file:
    params = json.load(params_file)

# Load params
settings = params['settings'][0]
status = params['status'][0]
climate = params['climate'][0]

mode = settings['mode']
setpoint = settings['setpoint']
deadband = settings['deadband']


def read_json():
    with open('params.json') as params_file:
        params = json.load(params_file)
    return params


def load_param(param_str='status'):
    params = read_json()
    param = params[param_str][0]
    return param


def set_cooling(cooling_status='off'):
    print('Cooling Status: {}'.format(cooling_status))

    if cooling_status == 'on':
        pass

    params = read_json()
    params['status'][0]['cooling'] = cooling_status
    with open('params.json', 'w') as outfile:
        json.dump(params, outfile, indent=4, sort_keys=True, separators=(',', ':'))


def check_climate(mode='off'):
    settings = load_param('settings')
    mode = settings['mode']

    current_temperature = read_temps(T)

    print('Current Temperature: {}*F'.format(current_temperature))
    print('Setpoint: {}*F'.format(setpoint))
    print('Deadband: {}*F'.format(deadband))

    if (mode == 'cool') and (current_temperature > setpoint + deadband / 2.0):
        set_cooling('on')

    if (mode == 'cool') and (current_temperature < setpoint - deadband / 2.0):
        set_cooling('off')

    print('Climate Checked')


def main():
    while True:
        time.sleep(DELAY_TIME)
        check_climate(mode)


if __name__ == "__main__":
    main()
