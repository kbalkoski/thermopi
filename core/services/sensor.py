try:
    import Adafruit_DHT

    test_mode = False
except:
    test_mode = True

import numpy as np

READ_DELAY = 0.5
AVERAGE_COUNT = 3

if not test_mode:
    DEFAULT_SENSOR = Adafruit_DHT.DHT11
else:
    DEFAULT_SENSOR = None

DEFAULT_PIN = 4


def c2f(x):
    return x * 9 / 5.0 + 32.0


class TempSensor:
    def __init__(self, name='', sensor=DEFAULT_SENSOR, pin=DEFAULT_PIN, offset=0):
        self.name = name
        self.sensor = sensor
        self.pin = pin
        self.T_now = -99
        self.T_list = [self.T_now, self.T_now, self.T_now]
        self.T = self.T_now
        self.offset = offset
        self._first = True

    def read_temperature(self):
        """
        Read the current temperature of this sensor. Does not update self.T_now
        :return: temperature
        """
        try:
            if not test_mode:
                humidity, temperature = c2f(Adafruit_DHT.read_retry(self.sensor, self.pin))
            else:
                temperature = 77.7
            temperature += self.offset
        except:
            print('Unable to read temperature')
            temperature = np.nan
        return temperature

    def _update_T_list(self):
        """
        Update list of temperatures
        """
        self.T_list.pop(0)
        self.T_list.append(self.T_now)
        if self._first:
            print('Initializing {}.T_list'.format(self.name))
            self.T_list[0] = self.T_now
            self.T_list[1] = self.T_now
            self._first = False

    def get_temp(self):
        """Read current temperature, update list, and return average
        :return: T
        """
        self.T_now = self.read_temperature()
        self._update_T_list()
        self.T = np.nanmean(self.T_list)
        return self.T


def read_temps(sensor_list, names=None):
    T = []
    for sensor in sensor_list:
        T_sense = sensor.get_temp()
        if (names is None) or (sensor.name in names):
            T.append(T_sense)

    return np.nanmean(T)



def read_temp(device_no=0):
    # pin = device_pins[device_no]
    # sensor = Adafruit_DHT.DHT11
    #
    # humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

    t1 = TempSensor(name="T1")
    temperature = t1.get_temp()

    return temperature
